'use strict';

const { app, BrowserWindow } = require('electron');
const path = require('path');

const createMainWindow = async () => {
    const window_ = new BrowserWindow({
        show: false,
        width: 800,
        height: 800,
        webPreferences: {
            nodeIntegration: true
        }
    });

    window_.on('ready-to-show', () => {
        window_.show();
    });

    await window_.loadFile(path.join(__dirname, 'index.html'));

    return window_;
};

(async () => {
    await app.whenReady();
    await createMainWindow();
})();
