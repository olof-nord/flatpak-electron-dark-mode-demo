# flatpak electron dark mode demo

This project is a small demo to understand why an Electron app does not display the top bar with the configured desktop
theme.

## Setup

For the packaging to work, `flatpak`, `flatpak-builder` needs to be installed.
For the electron bundle, `node` and `npm` is required - this can be installed with `nvm`.

Lastly, add the flathub repo:

```shell
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
```

## Build

Install dependencies and generate the electron bundle:

```shell
npm ci && npm run build
```

## Install & Run

Build and install the flatpak application:

```shell
make install
```

Finally, start the application:

```shell
make run
```

## Dark mode?

For example by toggling the developer mode found under View, one can see that the dark mode is indeed applied.

The problem can be seen by the top bar - it is light and not dark.

This problem stems from running an Electron app under Wayland.

## Solution 1: Hardcode host Legacy GTK theme (Wayland)

Set the host system Legacy GTK theme. GNOME by default uses the Adwaita theme. Its dark counterpart is Adwaita-dark.

(see /usr/share/themes).

We can install the dark theme as a flatpak package, and specify it on the host system:

```shell
flatpak install org.gtk.Gtk3theme.Adwaita-dark
gsettings set org.gnome.desktop.interface gtk-theme Adwaita-dark
```

To reset, use `gsettings set org.gnome.desktop.interface gtk-theme Adwaita`.

Thanks to Patrick on the [Flathub](https://matrix.to/#/#flathub:matrix.org) Matrix chat!

-> this is not "dark mode" it is a completely different theme.

## Solution 2: Run with X11

The Wayland support of Electron / Chrome for Dark mode is not yet ready.

By not providing the `--ozone-platform-hint=auto` flag on startup, it effectively starts with X11.

## Screenshots

Light mode (Wayland Adwaita)

![light mode](images/light-mode.png)

Manual dark mode (Wayland Adwaita-dark)

![hardcoded dark mode](images/dark-mode-hardcoded.png)

Automatic dark mode (X11 Adwaita)

![x11 dark mode](images/dark-mode-x11.png)

## Resources

* https://github.com/flatpak/flatpak-builder
* https://docs.flatpak.org/en/latest/flatpak-command-reference.html